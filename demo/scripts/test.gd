extends Node

# Injected dependencies
onready var logger : Logger  = Installer.inject(Logger)
onready var counterA1 : CounterA = Installer.inject(CounterA)
onready var counterA2 : CounterA = Installer.inject(CounterA)
onready var counterB1 : CounterB = Installer.inject(CounterB)
onready var counterB2 : CounterB = Installer.inject(CounterB)

func _ready():
	print("-------------------------------")
	logger.info("Using DI: printing through an interface")
	
	print("-------------------------------")
	logger.info("Incrementing only second A counter: A counters are installed as a single instance")
	counterA2.increment()
	logger.info("counter A1: " + String(counterA1.count))
	logger.info("counter A2: " + String(counterA2.count))
	
	print("-------------------------------")
	logger.info("Incrementing only second B counter: B counters are installed as transient instances")
	counterB2.increment()
	logger.info("counter B1: " + String(counterB1.count))
	logger.info("counter B2: " + String(counterB2.count))

