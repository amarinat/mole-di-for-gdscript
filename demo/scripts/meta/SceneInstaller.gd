extends Node

func _enter_tree():
	print("Installing or overriding scene specific bindings")
	Installer.bind(CounterA, CounterA, CompositionTree.InjectionType.SINGLE)
