class_name BaseInstaller

extends Node

var container : CompositionTree = CompositionTree.new()

func inject(type) :
	return container.inject(type)

func bind(interfaceType, implementationType, injectionType) -> void:
	container.bind(interfaceType, implementationType, injectionType)
