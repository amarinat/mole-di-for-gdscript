class_name CompositionTree

# Data structures
enum InjectionType {INVALID, SINGLE, TRANSIENT}
class InjectionEntry:
	var injectionType
	var functor

# Variables
var entries : Dictionary

# Installation method
func bind(interfaceType, implementationType, injectionType) -> void:
	var entry = InjectionEntry.new()
	entry.injectionType = injectionType
	match injectionType:
		InjectionType.SINGLE: entry.functor = implementationType.new()
		InjectionType.TRANSIENT: entry.functor = implementationType
		_: printerr("invalid instantiation type")
	entries[interfaceType] = entry

# Retrieval method
func inject(type) :
	var entry : InjectionEntry = entries[type]
	var instance
	match entry.injectionType:
		InjectionType.SINGLE: instance = entry.functor
		InjectionType.TRANSIENT: instance = entry.functor.new()
		_: printerr("invalid instantiation type")
	return instance
