extends BaseInstaller

func _enter_tree():
	print("Installing global bindings")
	bind(Logger, ConsoleLogger, CompositionTree.InjectionType.SINGLE)
	bind(CounterA, CounterA, CompositionTree.InjectionType.TRANSIENT)
	bind(CounterB, CounterB, CompositionTree.InjectionType.TRANSIENT)
