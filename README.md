# Mole Dependency Injection for Godot

This is a concept prototype. There is no help from the framework if something goes wrong!

## HOW TO USE IT

***This example requires Godot 3***.

Create your composition tree on enter tree through GlobalInstaller
- bind a type to itself using ''bind(TypeName, TypeName,...)''
- bind an implementation to is interface using ''bind(Interface, Implementation,...)''
- specify the binding type:
	- use ''bind(...,..., InjectionType.SINGLE)'' for binding a shared instance (singleton)
	- use ''bind(...,..., InjectionType.TRANSIENT)'' for binding separate instances
You can modify the composition tree at any point, for example through SceneInstaller

Inject the dependencies on ready or later through Installer
- use Installer.inject(TargetType) in order to fulfill the dependency

## HOW DOES IT WORK

The main class is CompositionTree (a.k.a. container). It binds types to the specified instantiation logic and manages their retrieval.

BaseInstaller serves as a wrapper node for the container. I could easily be skipped or merged into the other classes, but it is kept separated here for clarity reasons.

GlobalInstaller is an autoloaded specialization example of BaseInstaller. It can be used in order to install and share a single composition tree throughout the whole run.

SceneInstaller is a simple node that modifies the global installer upon entering its own scene. It could also be used as an independent installer for each given context, avoiding the autoloaded one altogether.

## PROVIDED EXAMPLES

Upon running the scene the console output should print the effects of the test scene.

The scene relies on 3 nodes with a single script:
- Installer: global installation script (autoloaded)
- DI Installer: scene specific installation script
- DI Consumer: script effectively using the dependency inversion pattern.

The effectively used scripts are the following ones:
- CounterA and CounterB are identical classes, separated only by their type name. They simply allow to show the differences between using SINGLE and TRANSIENT installation methods. SINGLE counters could be injected into several classes allowing for shared state, without any derived sharing responsibility outside the container.
- Logger represents an interface, that shows the possibility to inject abstract behavior without relying on the final implementations.
- ConsoleLogger represents an implementation, which could be swapped by a mock or an alternative implementation without any change outside of the installation process (think of eventual FileLogger or ScreenLogger implementations instead)
- test represents a simple class where dependency injection is performed. This class consumes an abstract interface (Logger) as well as several references to instances without needing to know details about their singleton resolution (CounterA, CounterB).
